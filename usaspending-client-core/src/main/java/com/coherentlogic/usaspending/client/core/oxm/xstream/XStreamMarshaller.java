package com.coherentlogic.usaspending.client.core.oxm.xstream;

import com.coherentlogic.usaspending.client.core.domain.Agency;
import com.coherentlogic.usaspending.client.core.domain.Basic;
import com.coherentlogic.usaspending.client.core.domain.Complete;
import com.coherentlogic.usaspending.client.core.domain.CongressionalDistrict;
import com.coherentlogic.usaspending.client.core.domain.Doc;
import com.coherentlogic.usaspending.client.core.domain.FiscalYear;
import com.coherentlogic.usaspending.client.core.domain.FiscalYears;
import com.coherentlogic.usaspending.client.core.domain.Low;
import com.coherentlogic.usaspending.client.core.domain.LowData;
import com.coherentlogic.usaspending.client.core.domain.LowRecord;
import com.coherentlogic.usaspending.client.core.domain.Medium;
import com.coherentlogic.usaspending.client.core.domain.MediumData;
import com.coherentlogic.usaspending.client.core.domain.MediumRecord;
import com.coherentlogic.usaspending.client.core.domain.MediumRecords;
import com.coherentlogic.usaspending.client.core.domain.Recipient;
import com.coherentlogic.usaspending.client.core.domain.Records;
import com.coherentlogic.usaspending.client.core.domain.Result;
import com.coherentlogic.usaspending.client.core.domain.SearchCriteria;
import com.coherentlogic.usaspending.client.core.domain.SearchCriterion;
import com.coherentlogic.usaspending.client.core.domain.Summary;
import com.coherentlogic.usaspending.client.core.domain.SummaryData;
import com.coherentlogic.usaspending.client.core.domain.SummaryRecord;
import com.coherentlogic.usaspending.client.core.domain.TopBean;
import com.coherentlogic.usaspending.client.core.domain.TopContractingAgencies;
import com.coherentlogic.usaspending.client.core.domain.TopKnownCongressionalDistricts;
import com.coherentlogic.usaspending.client.core.domain.TopRecipients;
import com.coherentlogic.usaspending.client.core.domain.Totals;
import com.coherentlogic.usaspending.client.core.domain.USASpendingSearchResults;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;

public class XStreamMarshaller extends org.springframework.oxm.xstream.XStreamMarshaller {

    /**
     * @see https://groups.google.com/forum/#!topic/xstream-user/wiKfdJPL8aY
     * 
     * @throws com.thoughtworks.xstream.security.ForbiddenClassException
     */
    @Override
    protected void customizeXStream(XStream xstream) {

        super.customizeXStream(xstream);

        xstream.addPermission(NoTypePermission.NONE);

        xstream.allowTypes(
            new Class[] {
                Agency.class,
                CongressionalDistrict.class,
                Doc.class,
                FiscalYear.class,
                FiscalYears.class,
                LowData.class,
                LowRecord.class,
                MediumData.class,
                MediumRecord.class,
                MediumRecords.class,
                Recipient.class,
                Records.class,
                Result.class,
                SearchCriteria.class,
                SearchCriterion.class,
                SummaryData.class,
                SummaryRecord.class,
                TopBean.class,
                TopContractingAgencies.class,
                TopKnownCongressionalDistricts.class,
                TopRecipients.class,
                Totals.class,
                USASpendingSearchResults.class,
                Summary.class,
                Basic.class,
                Low.class,
                Medium.class,
                Complete.class,
                SearchCriterion.class
            }
        );
    }
}
